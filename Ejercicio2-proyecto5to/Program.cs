﻿/*---------------------------
 2) Haz una clase llamada Persona que siga las siguientes condiciones:
    Los métodos que se implementaran son:
        calcularIMC(): calculara si la persona esta en su peso ideal (peso en kg/(altura^2  en m)), si esta fórmula devuelve un valor menor que 20, la función devuelve un -1, si devuelve un número entre 20 y 25 (incluidos), significa que esta por debajo de su peso ideal la función devuelve un 0  y si devuelve un valor mayor que 25 significa que tiene sobrepeso, la función devuelve un 1. Te recomiendo que uses constantes para devolver estos valores.
        esMayorDeEdad(): indica si es mayor de edad, devuelve un booleano.
        comprobarSexo(char sexo): comprueba que el sexo introducido es correcto. Si no es correcto, sera H. No sera visible al exterior.
        toString(): devuelve toda la información del objeto.
        generaDNI(): genera un número aleatorio de 8 cifras, genera a partir de este su número su letra correspondiente. Este método sera invocado cuando se construya el objeto. Puedes dividir el método para que te sea más fácil. No será visible al exterior.
 ---------------------------*/
using Microsoft.VisualBasic.CompilerServices;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Ejercicio1_proyecto5to
{
    class Persona
    {
        private string nombre;
        private int edad;
        private int dni;
        private char sexo = 'H';
        private double peso;
        private double altura;
        /*----------------------------
        Primer Constructor   
        ----------------------------*/
        public Persona()
        {

        }
        /*----------------------------
        Segundo Constructor   
        ----------------------------*/
        public Persona(string nombre, int edad, char sexo)
        {
            this.nombre = nombre;
            this.edad = edad;
            this.sexo = sexo;
        }
        /*-----------------------------
         *Tercer Constructor
        ------------------------------*/
        public Persona(string nombre, int edad, int dni, char sexo, double peso, double altura)
        {
            this.nombre = nombre;
            this.edad = edad;
            this.dni = dni;
            this.comprobarSexo();
            this.peso = peso;
            this.altura = altura;
            this.GenerarDni();
        }


        /*----------------------------
        Propiedades Getter  
        ----------------------------*/

        public string Nombre
        {
            get
            {
                return nombre;
            }
            set
            {
                this.nombre = value;

            }
        }

        public int Edad
        {
            get
            {
                return edad;
            }
            set
            {
                this.edad = value;

            }
        }

        public int DNI
        {
            get
            {
                return dni;
            }
        }
        public char Sexo
        {
            get
            {
                return sexo;
            }
            set
            {
                this.sexo = value;

            }
        }
        public double Peso
        {
            get
            {
                return peso;
            }
            set
            {
                this.peso = value;

            }
        }
        public double Altura
        {
            get
            {
                return altura;
            }
            set
            {
                this.altura = value;

            }
        }

        /*---------------------------
         METODOS
        ----------------------------*/
        public double peso_ideal (double  peso)
        {
            double pesoactual = peso / Math.Pow(altura, 2);
            int pesoideal = 0;
            int sobrepeso = 1;
            int infrapeso = -1;
            if (pesoactual >= 20 && pesoactual <= 25)
            {
                return pesoideal;    
            }
            else if (pesoactual < 20)
            {
                return infrapeso;
            }
            else
            {
                return sobrepeso;
            }
        }
        public bool mayorEdad()
        {
            bool edadbool = true;
            if (this.edad > 18)
            {
                edadbool = true;
            }
            else
            {
                edadbool = false;
            }
            return edadbool;
        }
        public void comprobarSexo()
        {
            if (!(this.sexo == 'H' || this.sexo == 'M'))
            {
                this.sexo = 'H';
            }
        }
        public void GenerarDni()
        {
            int dni = 0;
            Random aleatorio = new Random();

            for (int i = 1; i < 8; i++)
            {
               dni = +aleatorio.Next(0, 9);
            }
            Console.WriteLine(dni);
        }
    }
    class Program
    {
        static ConsoleKeyInfo tecla;
        static string nombre;
        static string edad;
        static string sexo;
        static string dni;
        static string peso;
        static string altura;
        static void Main(string[] args)
        {
            
                Console.Clear();
                Console.WriteLine("1. Ingresar nombre, edad, sexo, altura y peso");
                
                Console.WriteLine();
                Console.Write("Ingrese un nombre: ");
                nombre = Console.ReadLine();

                Console.WriteLine();
                Console.Write("Ingrese una edad: ");
                edad = Console.ReadLine();

                Console.WriteLine();
                Console.Write("Ingrese un sexo: ");
                sexo = Console.ReadLine();

                Console.WriteLine();
                Console.Write("Ingrese una peso: ");
                peso = Console.ReadLine();

                Console.WriteLine();
                Console.Write("Ingrese un altura: ");
                altura = Console.ReadLine();
                    
                string IdealPeso = Console.ReadLine();
                string CompSexo = Console.ReadLine();

                Persona p1 = new Persona (nombre, Convert.ToInt16(edad), Convert.ToInt16(dni), Convert.ToChar(sexo), Convert.ToInt16(peso), Convert.ToDouble(altura));
                Persona p2 = new Persona (nombre, Convert.ToInt16(edad), Convert.ToChar(sexo));
                Persona p3 = new Persona ();

                    
                double PesoIdeal = p1.peso_ideal(Convert.ToDouble(IdealPeso));
                Console.WriteLine(PesoIdeal);
                Console.ReadLine();

                p2.mayorEdad();
                Console.ReadLine();
        }
    }
}


