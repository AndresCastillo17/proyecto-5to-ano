﻿using System;
using System.Collections.Generic;

namespace Ejercicio9_proyecto5to
{
    class Cine
    {
        private List<List<Asiento>> asientos = new List<List<Asiento>>();
        private Pelicula pelicula;
        private float precio_entrada;
        private Cine(int filas, int columnas, float precio, Pelicula pelicula)
        {
            this.precio_entrada = precio;
            this.pelicula = pelicula;
            int columnas_t = (columnas > 26) ? 26 : columnas;    
        }
    }
    class Espectador
    {
        private string nombre;
        private int edad;
        private double dinero;

        private Espectador(string nombre, int edad, double dinero)
        {
            this.nombre = nombre;
            this.edad = edad;
            this.dinero = dinero;
        }

        public string Nombre
        {
            get
            {
                return this.nombre;
            }
            set
            {
                this.nombre = value;
            }
        }
        public int Edad
        {
            get
            {
                return this.edad;
            }
            set
            {
                this.edad = value;
            }
        }
        public double Dinero
        {
            get
            {
                return this.dinero;
            }
            set
            {
                this.dinero = value;
            }
        }
        public bool tieneEdad(int edad_minima)
        {
            if (edad >= edad_minima)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool tieneDinero(float precio_entrada)
        {
            if (dinero >= precio_entrada)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
    class Pelicula
    {
        private String titulo;
        private int duracion;
        private int edad_minima;
        private String director;
        public Pelicula(string titulo, int duracion, int edad_minima, string director)
        {
            this.titulo = titulo;
            this.duracion = duracion;
            this.edad_minima = edad_minima;
            this.director = director;
        }
        public string Titulo
        {
            get
            {
                return this.titulo;
            }
            set
            {
                this.titulo = value;
            }
        }
        public int Duracion
        {
            get
            {
                return this.duracion;
            }
            set
            {
                this.duracion = value;
            }

        }
        public int Edad_minima
        {
            get
            {
                return this.edad_minima;
            }
            set
            {
                this.edad_minima = value;
            }
        }
        public string Director
        {
            get
            {
                return this.director;
            }
            set
            {
                this.director = value;
            }
        }
        public string toString()
        {
            return String.Format("{0} del director {1}, con una duración de {2} minutos y la edad mínima es de {3} años.", this.titulo, this.director, this.duracion, this.edad_minima);

        }
    }
    class Asiento
    {
        private char letra;
        private int fila;
        private Espectador espectador;
        public Asiento(char letra, int fila)
        {
            this.letra = letra;
            this.fila = fila;
        }
        public char Letra
        {
            get
            {
                return this.letra;
            }
            set
            {
                this.letra = value;
            }

        }
        public int Fila
        {
            get
            {
                return this.fila;
            }
            set
            {
                this.fila = value;
            }

        }
        public Espectador Espectador
        {
            get
            {
                return this.espectador;
            }
            set
            {
                this.espectador = value;
            }
        }
        private bool ocupado()
        {
            return espectador != null;
        }
        public string toString()
        {
            if (ocupado())
            {
                return String.Format("Asiento: {0} y {1}", fila, letra);
            }
            else
            {
                return String.Format("Asiento: {0} y {1} está vacío", fila, letra);
            }
        }
    }
    class Program
    {
       static void Main(string[] args)
       {

       }
    }
}