﻿/*-------------------------------
 1) Crear una clase llamada "Cuenta" que tendrá los siguientes atributos: titular y cantidad (puede tener decimales).
    * El titular será obligatorio y la cantidad es opcional. Crea dos constructores que cumpla lo anterior.
    * Crea sus métodos get, set y toString.
    * Tendrá dos métodos especiales:
        ingresar(double cantidad): se ingresa una cantidad a la cuenta, si la cantidad introducida es negativa, no se hará nada.
        retirar(double cantidad): se retira una cantidad a la cuenta, si restando la cantidad actual a la que nos pasan es negativa, la cantidad de la cuenta pasa a ser 0.
--------------------------------*/
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace Ejercicio1_proyecto5to
{
    class cuenta
    {
        private string titular;
        private double cantidad;
        // static private cuenta respuesta;
        // static private List<cuenta> mi_lista = new List<cuenta>();


        /*----------------------------
        Primer Constructor   
        ----------------------------*/
        public cuenta(string titular)
        {
            this.titular = Titular;
        }

        /*----------------------------
        Segundo Constructor   
        ----------------------------*/
        public cuenta(string titular, double cantidad)
        {
            this.titular = Titular;
            this.cantidad = Cantidad;
        }

        /*----------------------------
        Propiedades Getter  
        ----------------------------*/

        public string Titular
        {
            get
            {
                return titular;
            }

        }

        public double Cantidad
        {
            get
            {
                return this.cantidad;
            }
            set
            {
                this.cantidad = value;

            }

        }
        /*---------------------------
         1er metodo del ejercicios
        ----------------------------*/
          public double ingresar (double cantidad)
          {
            if (cantidad > 0)
            {
                this.cantidad+= cantidad;
                Console.WriteLine("Cuanto quieres retirar?: ");
                string RetirarDinero = Console.ReadLine();
                double CuentaRetirar = this.retirar(Convert.ToDouble(RetirarDinero));
                Console.WriteLine(CuentaRetirar);
                Console.ReadLine();

            }
            else
            {
                this.cantidad = 0;
            }
            return this.cantidad;
          }
          
          public double retirar (double cantidad)
          {
            if (this.cantidad - cantidad < 0)
            {

                this.cantidad = 0;
                Console.WriteLine(this.cantidad + "Cuenta insuficiente");
                Console.ReadLine();

            }
            else
            {
                this.cantidad = this.cantidad - cantidad;
                
            }


            return this.cantidad;
          }
          

    }
    class Program
    {
        static ConsoleKeyInfo tecla;
        static string titular;
        static double cantidad;
        // static List<cuenta> lista_personas = new List<cuenta>();

        static void Main(string[] args)
        {
            while (true)
            {
                Console.Clear();
                Console.WriteLine("1. Ingrese un titular y cantidad");
                Console.WriteLine("2. Retirar una cantidad");
                tecla = Console.ReadKey();

                if (tecla.Key == ConsoleKey.D1)
                {
                    Console.WriteLine();
                    Console.Write("Ingrese un titular: ");
                    titular = Console.ReadLine();

                    Console.WriteLine();
                    Console.Write("Ingrese una cantidad: ");
                    string cantidadConsola = Console.ReadLine();
                    
                    cuenta c2 = new cuenta(titular, cantidad);
                   

                    double cuentatotal = c2.ingresar(Convert.ToDouble(cantidadConsola));
                    Console.WriteLine(cuentatotal);
                    Console.WriteLine("Listo papi sos millonario");
                    Console.ReadLine();
                 
                }
                /*if (tecla.Key == ConsoleKey.D2)
                {
                    Console.WriteLine();
                    Console.Write("Retirar una cantidad: ");
                    string cantidadConsola = Console.ReadLine();
                    cuenta c2 = new cuenta(titular, cantidad);

                    double cuentatotal = c2.retirar(Convert.ToDouble(cantidadConsola));
                    Console.WriteLine(cuentatotal);
                    Console.WriteLine("Listo papi sos pobre");
                    Console.ReadLine();

                }
               */
            }
        }
    }
}
