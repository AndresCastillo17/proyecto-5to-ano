﻿using System;

namespace Ejercicio6_proyecto5to
{
    class Libro
    {
        public int iSBN;
        public string titulo;
        public string autor;
        public int numero_de_paginas;

        public Libro (int iSBN, string titulo, string autor, int numero_de_paginas)
        {
            this.iSBN = iSBN;
            this.titulo = titulo;
            this.autor = autor;
            this.numero_de_paginas = numero_de_paginas;
        }

        public int ISBN
        {
            get
            {
                return this.iSBN;
            }
            set
            {
                this.iSBN = value;
            }
        }
        public string Titulo
        {
            get
            {
                return this.titulo;
            }
            set
            {
                this.titulo = value;
            }
        }
        public string Autor
        {
            get
            {
                return this.autor;
            }
            set
            {
                this.autor = value;
            }
        }
        public int Numero_de_paginas
        {
            get
            {
                return this.numero_de_paginas;
            }
            set
            {
                this.numero_de_paginas = value;
            }
        }
        public string toString()
        {
            return "El libro " + titulo + " con ISBN " + ISBN + "" + " creado por el autor " + autor + " tiene " + numero_de_paginas + " páginas";
        }
    }
    class Ejecutable
    {
        static void Main(string[] args)
        {

            Libro libro1 = new Libro(123, "Harry Potter: El caliz del fuego", "J.K. Rowling", 300);
            Libro libro2 = new Libro(111, "Cerebro del futuro", "Facundo Manes", 500);

            int LibroPaginas1 = libro1.Numero_de_paginas;
            string TituloLibro1 = libro1.Titulo;

            int LibroPaginas2 = libro2.Numero_de_paginas;
            string TituloLibro2 = libro2.Titulo;

            if (LibroPaginas1 < LibroPaginas2)
            {
                Console.WriteLine("El libro " + TituloLibro2 + " tiene " + LibroPaginas2 + " paginas ");
            } else if (LibroPaginas1 > LibroPaginas2) {
                Console.WriteLine("El libro " + TituloLibro1 + " tiene " + LibroPaginas1+ " paginas ");
            }
            else
            {
                Console.WriteLine("Tienen las mismas paginas");
            }

        }
    }
    

}
    