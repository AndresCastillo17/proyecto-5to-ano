﻿using System;

namespace Ejercicio8_proyecto5to
{
    public abstract class Persona
    {
        string nombre;
        int edad;
        char sexo;
        bool asistencia;

        /*-----------------------
              Constructores 
        *------------------------*/
        public Persona(string nombre, int edad, char sexo)
        {
            this.nombre = nombre;
            this.edad = edad;
            this.sexo = sexo;
            this.asistencia = this.disponibilidad();
        }
        /*-------------------------
          Metodos Getter y Setter 
        *------------------------*/
        public string Nombre
        {
            get
            {
                return this.nombre;
            }
            set
            {
                this.nombre = value;
            }
        }
        public int Edad
        {
            get
            {
                return this.edad;
            }
            set
            {
                this.edad = value;
            }
        }
        public char Sexo
        {
            get
            {
                return this.sexo;
            }
            set
            {
                this.sexo = value;
            }
        }
        public bool Asistencia
        {
            get
            {
                return this.asistencia;
            }
            set
            {
                this.asistencia = value;
            }
        }
        public abstract bool disponibilidad();
    }
    class Estudiantes : Persona
    {
        public int calificacion_actual;

       /*-----------------------
             Constructores 
       *------------------------*/
        public Estudiantes(string nombre, int edad, char sexo, int calificacion_actual) : base(nombre, edad, sexo) 
        {

        }

        /*-----------------------
          Metodo Getter y Setter
        *------------------------*/
        public int Calificacion_actual
        {
            get
            {
                return this.calificacion_actual;
            }
            set
            {
                this.calificacion_actual = value;
            }
        }
        /*-----------------------
                 Metodo 
        *------------------------*/
        public override bool disponibilidad()
        {
            Random rng = new Random();
            bool randomBool = rng.Next(0, 2) > 0;

            if (randomBool == true)
            {
                return true;
            }
            return false;
        }
        public string toString()
        {
            return String.Format("Nombre: {0} \n Sexo: {1} \n Nota: {2}", this.Nombre, this.Sexo, this.calificacion_actual);
        }
    }
    class Profesores : Persona
    {
        private string materia;

        /*-----------------------
             Constructores 
       *------------------------*/
        public Profesores(string nombre, int edad, char sexo, string materia) : base(nombre, edad, sexo)
        {
            this.materia = materia;
            this.Asistencia = this.disponibilidad();
        }
        /*-----------------------
         Metodos Getter y Setter
        *------------------------*/
        public string Materia
        {
            get
            {
                return this.materia;
            }
            set
            {
                this.materia = value;
            }
        }
        /*-----------------------
                Metodos 
        *------------------------*/
        public override bool disponibilidad()
        {
            Random rnd = new Random();
            int month = rnd.Next(1, 100);

            if (month < 20)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }
    class Aula
    {
        private int id;
        private Profesores profesor;
        private Estudiantes[] alumnos;
        private string materia;
        private const int MAX_ALUMNOS = 20;

        public Aula(int id, Profesores profesor, Estudiantes[] alumnos, string materia, int MAX_ALUMNOS)
        {
            this.id = id;
            this.profesor = profesor;
            this.alumnos = alumnos;
            this.materia = materia;
        }
        /*-----------------------
               Metodos 
       *------------------------*/
        private bool van_alumnos()
        {
            int contador = 0;
            for (int i = 0; i < alumnos.Length; i++)
            {
                if (alumnos[i].disponibilidad())
                {
                    contador++;
                }
            }
            if (contador > alumnos.Length / 2)
            {
                return true;
            }
            return false;
        }
        public bool dar_clase()
        {
            if (!profesor.disponibilidad())
            {
                Console.WriteLine("Hoy el profesor no va a asistir :), pueden retirarse");
                return false;
            }
            else if (profesor.Materia != this.materia)
            {
                return false;
            }
            else if (this.van_alumnos())
            {
                Console.WriteLine("Se podra dar clase");
                return true;
            }
            else
            {
                return false;
            }
        }
        public string notas()
        {
            int contador_H = 0;
            int contador_M = 0;

            for (int i = 0; i < alumnos.Length; i++)
            {
                if (alumnos[i].Calificacion_actual >= 6 && alumnos[i].Sexo == 'H')
                {

                    contador_H++;
                }

                if (alumnos[i].Calificacion_actual >= 6 && alumnos[i].Sexo == 'M')
                {
                    contador_M++;
                }
            }
            return String.Format("Hay {0} alumnos aprobados y {1} chicas aprobadas", contador_H, contador_M);
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Estudiantes[] estudiantes = new Estudiantes[3];
            estudiantes[0] = new Estudiantes("Andres Castillo", 19, 'H', 10);
            estudiantes[1] = new Estudiantes("Franco Del Medico", 17, 'M', 6);
            estudiantes[2] = new Estudiantes("Jeremy Nuñez", 18, 'M', 8);
            estudiantes[3] = new Estudiantes("Ismael Scorzelli", 18, 'M', 10);

            Profesores profesor = new Profesores("Pablo Fiscella", 30, 'H', "POO");

            Aula obj_aula = new Aula(0, profesor, estudiantes, "POO", 20);

            if (obj_aula.dar_clase())
            {
                obj_aula.notas();
            }
        }
    }
}
